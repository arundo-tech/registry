# registry

arundo.tech public registry

## Yarn use

To use package published in this repo with Yarn :

```shell
echo @arundo-tech:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc

# add a package to your project
yarn add @arundo-tech/paxpar-widgets
yarn add @arundo-tech/supabase
```


## pnpm use

To use package published in this repo with Yarn :

```shell
echo @arundo-tech:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc

# add a package to your project
pnpm add @arundo-tech/paxpar-widgets
pnpm add @arundo-tech/supabase
```

## NPM/Yarn publish

Create `.npmrc` :
```
@arundo-tech:registry=https://gitlab.com/api/v4/projects/45237886/packages/npm/
//gitlab.com/api/v4/projects/45237886/packages/npm/:_authToken="${CI_JOB_TOKEN}"
always-auth=true
```
Then :
```shell
# Set a valid token
export CI_JOB_TOKEN=xxx
# publish with npm
npm publish
# ... or yarn
yarn publish
```
Usefull in .npmrc ?
```
always-auth=true
```

Uselfull in packages.json ? :
```json
  "publishConfig": {
    "registry": "https://gitlab.com/api/v4/projects/45214051/packages/npm/"
  }
```
